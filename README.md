## Forgejo Snap packaging

[![forgejo](https://snapcraft.io/forgejo/badge.svg)](https://snapcraft.io/forgejo)

This repository contains a work in progress packaging recipe to build a snap of Forgejo for publishing in the Snap Store at https://snapcraft.io/forgejo

Please see [#2542](https://codeberg.org/forgejo/forgejo/issues/2542) for discussion.

### Installation

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/forgejo)

`snap install forgejo`

### Coinfiguration

Visit https://localhost:3000/ to begin initial configuration.
